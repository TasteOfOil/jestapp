//GET


test('GET lesson',async ()=>{
    const getLesson = await fetch('http://localhost:3000/api/lessons/6').then(res => res.json()).catch(err=>'error');
    expect.assertions(1);
    expect(getLesson[0].lessonname).toEqual("Algebra");
});


//POST
var lesson = {
    lessonName: "Art",
    teacherName: "Galina Sergeevna",
    studentCount: 120
};



test('POST lesson', async()=>{
    const result = await fetch('http://localhost:3000/api/lessons', {
        method: 'POST',
        body: JSON.stringify(lesson),
        headers: { 'Content-Type': 'application/json' }
    }).then(res => res.text());
    expect.assertions(1);
    expect(result).toEqual("Data added successfully");
});

//PUT
var newLesson = {
    lessonName: "English"
};

test('PUT lesson', async()=>{
    const result = await fetch('http://localhost:3000/api/lessons/6', {
        method: 'PUT',
        body: JSON.stringify(newLesson),
        headers: { 'Content-Type': 'application/json' }
    }).then(res => res.text());
    expect.assertions(1);
    expect(result).toEqual("Data updated successfully");
});

//DELETE

test('DELETE lesson', async()=>{
    const result = await fetch('http://localhost:3000/api/lessons/8', {
        method: 'DELETE'
    }).then(res => res.text());
    expect.assertions(1);
    expect(result).toEqual("Data deleted successfully");
});

//GET all

test('GET lessons',async ()=>{
    const getLessons = await fetch('http://localhost:3000/api/lessons').then(res => res.json()).catch(err=>'error');
    expect(getLessons).not.toContain({
        id: 7,
        lessonname: "Geometry",
        teachername: "Michail Petrovich",
        studentcount: 140
    });
});
